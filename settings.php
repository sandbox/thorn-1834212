<?php
/**
 * Page callback: Commerce E-conomics settings
 *
 * @see commerce_e_conomics_menu()
 */
function e_conomics_form($form, &$form_state){

	$form['agreement_number'] = array(
			'#type' => 'textfield',
			'#title' => t('Agreement Number'),
			'#size' => 50,
			'#maxlength' => 50,
			'#description' => t('Enter your Agreement Number provided by E-Conomic.'),
			'#required' => TRUE,
			);

	$form['user_id'] = array(
			'#type' => 'textfield',
			'#title' => t('User-ID'),
			'#size' => 50,
			'#maxlength' => 50,
			'#description' => t('Enter your User-ID provided by E-Conomic.'),
			'#required' => TRUE,
			);

	$form['password'] = array(
			'#type' => 'password',
			'#title' => t('Password'),
			'#size' => 50,
			'#maxlength' => 50,
			'#description' => t('Enter your Password provided by E-Conomic.'),
			'#required' => TRUE,
			);

	return system_settings_form($form);	
}
?>
